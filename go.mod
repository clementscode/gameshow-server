module gitlab.com/clementscode/gameshow-server

go 1.19

require github.com/go-chi/chi v4.0.2+incompatible

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/go-chi/cors v1.2.1 // indirect
	github.com/go-chi/render v1.0.2 // indirect
	github.com/golang-jwt/jwt/v4 v4.5.0 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/unrolled/render v1.6.0 // indirect
	golang.org/x/sys v0.0.0-20220908164124-27713097b956 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
