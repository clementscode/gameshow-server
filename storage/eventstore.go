package storage

import "gitlab.com/clementscode/gameshow-server/event"

type EventStore interface {
	Store(message event.RawEventMessage) error
	Query(start, end int) ([]event.RawEventMessage, error)
}

type MemoryEventStore struct {
	events []event.RawEventMessage
}

func (mes *MemoryEventStore) Store(message event.RawEventMessage) error {
	mes.events = append(mes.events, message)
	return nil
}

func (mes *MemoryEventStore) Query(start, end int) ([]event.RawEventMessage, error) {
	if start < 0 {
		start = 0
	}
	if end > len(mes.events) {
		end = len(mes.events)
	}
	return mes.events[start:end], nil
}
