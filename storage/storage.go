package storage

import "fmt"

type Storage interface {
	List() ([]string, error)
	Get(gameId string) (GameData, error)
}

func GetStorage(name string, config map[string]string) (result Storage, err error) {
	switch name {
	case "filesystem":
		storageRoot, ok := config["storageRoot"]
		if !ok {
			err = fmt.Errorf("Missing storageRoot entry in config")
		} else {
			result = CreateFileStorage(storageRoot)
		}
	default:
		err = fmt.Errorf("Unsupported storage: %s", name)
	}
	return
}

type GameData struct {
	Type    string   `json:"type"`
	Name    string   `json:"name"`
	GameId  string   `json:"id"`
	Prompts []Prompt `json:"prompts"`
}

type Prompt struct {
	Text            string `json:"text"`
	CorrectResponse string `json:"correctResponse"`
	ImageFile       string `json:"imageFile"`
}
