package storage

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
)

type FileStorage struct {
	Path      string
	FileRegex *regexp.Regexp
}

func CreateFileStorage(path string) FileStorage {
	r := regexp.MustCompile("(.*)\\.json")
	return FileStorage{path, r}
}

func (fs FileStorage) List() (results []string, err error) {
	files, err := os.ReadDir(fs.Path)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		groups := fs.FileRegex.FindStringSubmatch(file.Name())
		if groups != nil {
			results = append(results, groups[1])
		}
	}
	return
}

func (fs FileStorage) Get(gameId string) (gm GameData, err error) {
	path := filepath.Join(fs.Path, gameId+".json")
	bytes, err := readBytes(path)
	err = json.Unmarshal(bytes, &gm)
	return
}

func readBytes(path string) (bytes []byte, err error) {
	file, err := os.Open(path)
	if err != nil {
		return
	}
	defer file.Close()
	return ioutil.ReadAll(file)
}
