package main

import (
	"io/ioutil"
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"github.com/unrolled/render"
	"gitlab.com/clementscode/gameshow-server/auth"
	"gitlab.com/clementscode/gameshow-server/game"
	"gitlab.com/clementscode/gameshow-server/storage"
	"gopkg.in/yaml.v3"
)

type Config struct {
	Address         string            `yaml:"address"`
	StorageImpl     string            `yaml:"storageImpl"`
	StorageConfig   map[string]string `yaml:"storageConfig"`
	UserStoreImpl   string            `yaml:"userStoreImpl"`
	UserStoreConfig map[string]string `yaml:"userStoreConfig"`
	JwtKeyFile      string            `yaml:"jwtKeyFile"`
}

type WebStorage struct {
	storage.Storage
	render *render.Render
}

func (ws WebStorage) GetGameIds(w http.ResponseWriter, r *http.Request) {
	ids, err := ws.List()
	if err != nil {
		log.Println(err)
		ws.render.Text(w, 500, "Internal Server Error")
	} else {
		ws.render.JSON(w, 200, ids)
	}
}

func (ws WebStorage) GetGameData(w http.ResponseWriter, r *http.Request) {
	gameId := chi.URLParam(r, "gameId")
	game, err := ws.Get(gameId)
	if err != nil {
		log.Println(err)
		ws.render.Text(w, 500, "Internal Server Error")
	} else {
		ws.render.JSON(w, 200, game)
	}
}

func main() {

	configPath := "config.yml"
	if len(os.Args) > 1 {
		configPath = os.Args[1]
	}

	configFile, err := ioutil.ReadFile(configPath)
	if err != nil {
		log.Fatal(err)
	}

	var config Config
	err = yaml.Unmarshal(configFile, &config)
	if err != nil {
		log.Fatal(err)
	}

	storage, err := storage.GetStorage(config.StorageImpl, config.StorageConfig)
	if err != nil {
		log.Fatal(err)
	}

	renderer := render.New()
	webStorage := WebStorage{storage, renderer}
	r := chi.NewRouter()

	userStore, err := auth.GetUserStore(config.UserStoreImpl, config.UserStoreConfig)
	if err != nil {
		log.Fatal(err)
	}

	jwtKey, err := ioutil.ReadFile(config.JwtKeyFile)
	if err != nil {
		log.Fatal(err)
	}
	webAuth := auth.NewWebAuth(userStore, jwtKey)

	r.Use(cors.Handler(cors.Options{
		// AllowedOrigins:   []string{"https://foo.com"}, // Use this to allow specific origin hosts
		AllowedOrigins: []string{"https://*", "http://*"},
		// AllowOriginFunc:  func(r *http.Request, origin string) bool { return true },
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}))

	manager := game.NewManager(webAuth, renderer)

	r.Route("/api", func(r chi.Router) {
		r.Get("/gamedata", webAuth.WithRoles([]string{game.HostRole, game.DisplayRole}, webStorage.GetGameIds))
		r.Get("/gamedata/{gameId}", webAuth.WithRole(game.HostRole, webStorage.GetGameData))
		r.Post("/login", webAuth.Login)
		r.Get("/memberships", manager.GetMemberships)
		r.Get("/rooms", manager.ListRooms)
		r.Post("/rooms", webAuth.WithRole(game.HostRole, manager.CreateRoom))
		r.Post("/rooms/{roomId}", manager.AddClient)
		r.Delete("/rooms/{roomId}", webAuth.WithRole(game.HostRole, manager.DeleteRoom))
		r.Get("/rooms/{roomId}/data", webAuth.WithRole(game.HostRole, func(w http.ResponseWriter, r *http.Request) {
			log.Println("here")
			roomId := chi.URLParam(r, "roomId")
			info, ok := manager.GetInfo(roomId)
			if !ok {
				log.Printf("Requested missing room %v\n", roomId)
				w.WriteHeader(http.StatusNotFound)
				return
			}
			data, err := webStorage.Get(info.Game)
			if err != nil {
				log.Println(err)
				renderer.Text(w, 500, "Internal Server Error")
			} else {
				renderer.JSON(w, 200, data)
			}
		}))
	})

	r.Route("/ws", func(r chi.Router) {
		r.Get("/{roomId}/{role}", manager.WebSocketHandler)
	})

	log.Fatal(http.ListenAndServe(config.Address, r))
}
