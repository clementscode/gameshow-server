package event

import (
	"encoding/json"
	"log"
)

type GameEvent interface {
}

type BaseMessage struct {
	Type    string `json:"type"`
	Subject string `json:"subject"`
}

type EventMessage struct {
	BaseMessage
	Event GameEvent `json:"event"`
}

type RawEventMessage struct {
	BaseMessage
	Payload json.RawMessage `json:"event"`
}

func PartialSerialize(event EventMessage) (raw RawEventMessage, err error) {
	raw.BaseMessage = event.BaseMessage
	bytes, err := json.Marshal(event.Event)
	if err == nil {
		raw.Payload = json.RawMessage(bytes)
	}
	return
}

const ChangePrompt = "ChangePrompt"

type ChangePromptEvent struct {
	PromptIndex int `json:"promptIndex"`
}

const JoinedRoom = "JoinedRoom"

func NewJoinedRoomEvent(clientId string) RawEventMessage {
	return RawEventMessage{BaseMessage: BaseMessage{JoinedRoom, clientId}}
}

const ChangePoints = "ChangePoints"

type ChangePointsEvent struct {
	Change int `json:"change"`
}

const BuzzerPressed = "BuzzerPressed"
const BuzzerReset = "BuzzerReset"
const BuzzerAccepted = "BuzzerAccepted"

func NewBuzzerAcceptedEvent(clientId string) (rem RawEventMessage) {
	return RawEventMessage{BaseMessage: BaseMessage{BuzzerAccepted, clientId}}
}

const PlayerState = "PlayerState"

type PlayerStateEvent struct {
	ClientId string `json:"clientId"`
	Points   int    `json:"points"`
	OnStage  bool   `json:"onStage"`
	BuzzedIn bool   `json:"buzzedIn"`
}

func NewPlayerStateEvent(clientId string, points int, onStage, buzzedIn bool) (rem RawEventMessage) {
	rem.BaseMessage = BaseMessage{Type: PlayerState, Subject: clientId}
	playerState := PlayerStateEvent{clientId, points, onStage, buzzedIn}
	bytes, err := json.Marshal(playerState)
	if err == nil {
		rem.Payload = json.RawMessage(bytes)
	} else {
		log.Printf("Problem serializing %v: %v\n", PlayerState, err)
	}
	return
}

const RoomState = "RoomState"

type RoomStateEvent struct {
	PromptIndex int                `json:"promptIndex"`
	Players     []PlayerStateEvent `json:"players"`
}

func NewRoomStateEvent(promptIndex int, players []PlayerStateEvent) (rem RawEventMessage) {
	rem.BaseMessage = BaseMessage{Type: RoomState}
	roomState := RoomStateEvent{promptIndex, players}
	bytes, err := json.Marshal(roomState)
	if err == nil {
		rem.Payload = json.RawMessage(bytes)
	} else {
		log.Printf("Problem serializing %v: %v\n", RoomState, err)
	}
	return
}

const SetStagePresence = "SetStagePresence"

type SetStagePresenceEvent struct {
	OnStage bool `json:"onStage"`
}

const EndGame = "EndGame"

const CloseRoom = "CloseRoom"

func NewCloseRoomEvent() (rem RawEventMessage) {
	rem.BaseMessage = BaseMessage{Type: CloseRoom}
	return
}
