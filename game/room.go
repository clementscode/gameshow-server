package game

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"

	"github.com/gorilla/websocket"
	"gitlab.com/clementscode/gameshow-server/event"
	"gitlab.com/clementscode/gameshow-server/storage"
)

const HostRole = "host"
const DisplayRole = "display"
const PlayerRole = "player"

type MessageRule struct {
	Senders   map[string]bool
	Receivers map[string]bool
}

var messageRules = map[string]MessageRule{
	event.ChangePrompt: {
		Senders:   map[string]bool{HostRole: true},
		Receivers: map[string]bool{DisplayRole: true, HostRole: true},
	},
	event.ChangePoints: {
		Senders:   map[string]bool{HostRole: true},
		Receivers: map[string]bool{DisplayRole: true, PlayerRole: true},
	},
	event.BuzzerPressed: {
		Senders:   map[string]bool{PlayerRole: true},
		Receivers: map[string]bool{},
	},
	event.BuzzerAccepted: {
		Senders:   map[string]bool{},
		Receivers: map[string]bool{HostRole: true, DisplayRole: true, PlayerRole: true},
	},
	event.BuzzerReset: {
		Senders:   map[string]bool{HostRole: true},
		Receivers: map[string]bool{DisplayRole: true, PlayerRole: true},
	},
	event.JoinedRoom: {
		Senders:   map[string]bool{},
		Receivers: map[string]bool{DisplayRole: true, HostRole: true},
	},
	event.PlayerState: {
		Senders:   map[string]bool{},
		Receivers: map[string]bool{PlayerRole: true},
	},
	event.RoomState: {
		Senders:   map[string]bool{},
		Receivers: map[string]bool{DisplayRole: true, HostRole: true},
	},
	event.EndGame: {
		Senders:   map[string]bool{HostRole: true},
		Receivers: map[string]bool{DisplayRole: true, PlayerRole: true},
	},
	event.SetStagePresence: {
		Senders:   map[string]bool{HostRole: true},
		Receivers: map[string]bool{DisplayRole: true},
	},
	event.CloseRoom: {
		Senders:   map[string]bool{},
		Receivers: map[string]bool{},
	},
}

type Client struct {
	Id          string `json:"id"`
	Points      int
	OnStage     bool
	Connections map[string]ClientConnection
}

type ClientConnection struct {
	Role    string
	Sockets map[string]*websocket.Conn
}

func (c Client) connected() bool {
	return len(c.Connections) > 0
}

type GameRoomInfo struct {
	Id   string `json:"id"`
	Host string `json:"host"`
	Game string `json:"game"`
}

type IncomingMessage struct {
	ClientId string
	Event    event.RawEventMessage
}

type GameRoom struct {
	mu           sync.RWMutex
	Info         GameRoomInfo
	Clients      map[string]*Client
	eventChannel chan IncomingMessage
	events       storage.EventStore
	buzzedPlayer string
	promptIndex  int
	closed       bool
}

func CreateGameRoom(info GameRoomInfo) *GameRoom {
	gr := &GameRoom{}
	gr.Info = info
	gr.Clients = make(map[string]*Client)
	gr.eventChannel = make(chan IncomingMessage)
	gr.events = &storage.MemoryEventStore{}
	// -1 shows the welcome message
	gr.promptIndex = -1
	go gr.Run()
	return gr
}

func (gr *GameRoom) AddClient(id string) bool {
	gr.mu.Lock()
	defer gr.mu.Unlock()
	if gr.closed {
		return false
	}
	_, exists := gr.Clients[id]
	if exists {
		return false
	}
	client := &Client{id, 0, true, make(map[string]ClientConnection)}
	gr.Clients[id] = client
	return true
}

func (gr *GameRoom) AddConnection(id, role string, conn *websocket.Conn) (created bool) {
	gr.mu.Lock()
	defer gr.mu.Unlock()
	if gr.closed {
		return false
	}
	log.Printf("Adding %v client for %v\n", role, id)
	client, exists := gr.Clients[id]
	if !exists {
		log.Printf("Connection before add from %v\n", id)
		created = true
		client = &Client{id, 0, true, make(map[string]ClientConnection)}
		gr.Clients[id] = client
	}

	newRole := false
	roleCon, ok := client.Connections[role]
	if !ok {
		newRole = true
		roleCon = ClientConnection{role, make(map[string]*websocket.Conn)}
		client.Connections[role] = roleCon
	}
	socketId := fmt.Sprintf("%s-%s-%d", id, role, len(roleCon.Sockets))
	roleCon.Sockets[socketId] = conn

	handler := func(code int, text string) error {
		log.Printf("Socket %s disconnected\n", socketId)
		gr.mu.Lock()
		defer gr.mu.Unlock()
		roleCon, ok := client.Connections[role]
		if ok {
			delete(roleCon.Sockets, socketId)
		}
		return nil
	}
	conn.SetCloseHandler(handler)
	go socketListen(roleCon, id, socketId, gr.eventChannel)
	if role == PlayerRole {
		if newRole {
			go gr.routeEvent(event.NewJoinedRoomEvent(id))
		}
		rule := messageRules[event.PlayerState]
		sendEvent(event.NewPlayerStateEvent(id, client.Points, client.OnStage, id == gr.buzzedPlayer), client, rule)
	} else {
		rule := messageRules[event.RoomState]
		sendEvent(event.NewRoomStateEvent(gr.promptIndex, gr.getPlayerState()), client, rule)
	}
	return
}

func (gr *GameRoom) getPlayerState() (players []event.PlayerStateEvent) {
	for clientId, client := range gr.Clients {
		_, ok := client.Connections[PlayerRole]
		if ok {
			players = append(players, event.PlayerStateEvent{
				ClientId: clientId,
				Points:   client.Points,
				OnStage:  client.OnStage,
				BuzzedIn: gr.buzzedPlayer == clientId,
			})
		}
	}
	return
}

func socketListen(c ClientConnection, clientId, socketId string, events chan IncomingMessage) {
	socket := c.Sockets[socketId]
	for {
		_, data, err := socket.ReadMessage()
		if err != nil {
			log.Printf("Problem reading message from %s: %v\n", socketId, err)
			return
		}
		var rawMessage event.RawEventMessage
		err = json.Unmarshal(data, &rawMessage)
		if err != nil {
			log.Printf("Problem parsing message from %s: %v\n", socketId, err)
			continue
		}
		rule, ok := messageRules[rawMessage.Type]
		if !ok {
			log.Printf("Unrecognized message type from %s: %v\n", socketId, rawMessage.Type)
			continue
		}
		if !rule.Senders[c.Role] {
			log.Printf("Sender %s not allowed to send type %s\n", socketId, rawMessage.Type)
			continue
		}
		log.Printf("Got %s message from %s\n", rawMessage.Type, socketId)
		events <- IncomingMessage{clientId, rawMessage}
	}
}

func (gr *GameRoom) Close() {
	gr.mu.Lock()
	defer gr.mu.Unlock()
	if gr.closed {
		return
	}
	gr.closed = true
	defer close(gr.eventChannel)
	gr.eventChannel <- IncomingMessage{"", event.NewCloseRoomEvent()}
	for _, client := range gr.Clients {
		for _, conn := range client.Connections {
			for _, socket := range conn.Sockets {
				err := socket.Close()
				if err != nil {
					log.Printf("Failed to close socket for %s as %s\n", client.Id, conn.Role)
				}
			}
		}
	}
}

func (gr *GameRoom) Run() {
	for !gr.closed {
		m := <-gr.eventChannel
		switch m.Event.Type {
		case event.CloseRoom:
			gr.closed = true
			return
		case event.BuzzerPressed:
			if gr.buzzedPlayer == "" {
				client := gr.Clients[m.ClientId]
				if client.OnStage {
					gr.buzzedPlayer = m.ClientId
					m.Event = event.NewBuzzerAcceptedEvent(m.ClientId)
				}
			}
		case event.BuzzerReset:
			gr.buzzedPlayer = ""
		case event.ChangePoints:
			var changePointsEvent event.ChangePointsEvent
			err := json.Unmarshal(m.Event.Payload, &changePointsEvent)
			if err != nil {
				log.Printf("Problem deserializing change points event %v\n", err)
				return
			}
			client, ok := gr.Clients[m.Event.Subject]
			if !ok {
				log.Printf("Tried to change points of unknown client %v\n", m.Event.Subject)
				return
			}
			_, ok = client.Connections[PlayerRole]
			if !ok {
				log.Printf("Tried to change points of non-player client %v\n", m.Event.Subject)
				return
			}
			client.Points += changePointsEvent.Change
		case event.ChangePrompt:
			var changePromptEvent event.ChangePromptEvent
			err := json.Unmarshal(m.Event.Payload, &changePromptEvent)
			if err != nil {
				log.Printf("Problem deserializing change prompt event %v\n", err)
				return
			}
			gr.promptIndex = changePromptEvent.PromptIndex
		case event.SetStagePresence:
			var setPresenceEvent event.SetStagePresenceEvent
			err := json.Unmarshal(m.Event.Payload, &setPresenceEvent)
			if err != nil {
				log.Printf("Problem deserializing change points event %v\n", err)
				return
			}
			client, ok := gr.Clients[m.Event.Subject]
			if !ok {
				log.Printf("Tried to set stage presence of unknown client %v\n", m.Event.Subject)
				return
			}
			_, ok = client.Connections[PlayerRole]
			if !ok {
				log.Printf("Tried to set stage presence of non-player client %v\n", m.Event.Subject)
				return
			}
			client.OnStage = setPresenceEvent.OnStage
		}
		gr.routeEvent(m.Event)
	}
}

func (gr *GameRoom) routeEvent(event event.RawEventMessage) {
	rule := messageRules[event.Type]
	gr.mu.RLock()
	defer gr.mu.RUnlock()
	for _, client := range gr.Clients {
		sendEvent(event, client, rule)
	}
}

func sendEvent(event event.RawEventMessage, client *Client, rule MessageRule) {
	for _, conn := range client.Connections {
		if rule.Receivers[conn.Role] {
			for socketId, socket := range conn.Sockets {
				log.Printf("Sending event %s to %s\n", event.Type, socketId)
				socket.WriteJSON(event)
			}
		} else {
			log.Printf("Not sending event %s to %s\n", event.Type, client.Id)
		}
	}
}
