package game

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/go-chi/chi"
	"github.com/gorilla/websocket"
	"github.com/unrolled/render"
	"gitlab.com/clementscode/gameshow-server/auth"
	"gitlab.com/clementscode/gameshow-server/event"
)

type Manager struct {
	auth.WebAuth
	mu       sync.RWMutex
	Rooms    map[string]*GameRoom
	upgrader websocket.Upgrader
	renderer *render.Render
}

func NewManager(wa auth.WebAuth, renderer *render.Render) Manager {
	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		CheckOrigin: func(r *http.Request) bool {
			return true
		},
	}
	return Manager{wa, sync.RWMutex{}, make(map[string]*GameRoom), upgrader, renderer}
}

func (gm *Manager) WebSocketHandler(w http.ResponseWriter, r *http.Request) {
	roomId := chi.URLParam(r, "roomId")
	role := chi.URLParam(r, "role")
	log.Printf("Got a websocket request for %v\n", roomId)
	claims, found, err := auth.ProcessClientToken(gm.JwtKey, r)
	if !found {
		err = http.ErrNoCookie
	}
	if err != nil {
		log.Printf("Token error: %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	memberInfo, ok := claims.RoomMap[roomId]
	if !ok {
		log.Printf("Room %v not found in claims for %v\n", roomId, memberInfo.ClientId)
		w.WriteHeader(http.StatusForbidden)
		return
	}
	if !memberInfo.HasRole(role) {
		log.Printf("Client %v tried to join with role %v\n", memberInfo.ClientId, role)
		w.WriteHeader(http.StatusForbidden)
		return
	}
	gm.mu.Lock()
	defer gm.mu.Unlock()
	room, ok := gm.Rooms[roomId]
	if !ok {
		log.Printf("Room %v not found in server\n", roomId)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	conn, err := gm.upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("Open error: %v\n", err)
		return
	}
	room.AddConnection(memberInfo.ClientId, role, conn)
	log.Printf("Client %v connected to room %v\n", memberInfo.ClientId, roomId)
}

func sendAndExpectMessage(conn *websocket.Conn, toSend event.EventMessage, response *event.RawEventMessage) error {
	err := conn.WriteJSON(toSend)
	if err != nil {
		err = expectMessage(conn, response)
	}
	return err
}

func expectMessage(conn *websocket.Conn, message *event.RawEventMessage) error {
	conn.SetReadDeadline(time.Now().Add(30 * time.Second))
	// reset connection timeout before returning
	defer conn.SetReadDeadline(time.Time{})
	return conn.ReadJSON(&message)
}

type ClientInfo struct {
	Id   string `json:"id"`
	Role string `json:"role"`
}

func (gm *Manager) AddClient(w http.ResponseWriter, r *http.Request) {
	roomId := chi.URLParam(r, "roomId")
	var client ClientInfo
	err := json.NewDecoder(r.Body).Decode(&client)
	if err != nil {
		log.Printf("Problem reading client info %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	clientId := client.Id
	var roles []string
	if client.Role != PlayerRole {
		userClaims, found, err := auth.ProcessUserToken(gm.JwtKey, r)
		if err != nil {
			log.Printf("User token error on add client: %v\n", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if !found || !userClaims.HasRole(client.Role) {
			log.Printf("User %v missing role %v\n", userClaims.Username, client.Role)
			w.WriteHeader(http.StatusForbidden)
			return
		}
		roles = userClaims.Roles
		clientId = userClaims.Username
	} else {
		roles = []string{PlayerRole}
	}

	gm.mu.Lock()
	defer gm.mu.Unlock()
	room, ok := gm.Rooms[roomId]
	if !ok {
		log.Printf("Asked for token for non-existant room %v\n", roomId)
		w.WriteHeader(http.StatusNotFound)
		return
	}
	clientClaims, found, err := auth.ProcessClientToken(gm.JwtKey, r)
	if err != nil {
		log.Printf("Client token error for %v: %v", clientId, err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	added := room.AddClient(clientId)
	if !added {
		if found && clientClaims.IsMember(roomId, clientId) {
			log.Printf("%v is already a member of the room %v\n", clientId, roomId)
			return
		}
		log.Printf("Name conflict on %v for room %v\n", clientId, roomId)
		w.WriteHeader(http.StatusConflict)
		return
	}
	var tokenString string
	var expirationTime time.Time
	if found {
		tokenString, expirationTime, err =
			auth.UpdateClientToken(roomId, clientId, roles, clientClaims, gm.JwtKey)
	} else {
		tokenString, expirationTime, err =
			auth.NewClientToken(roomId, clientId, roles, gm.JwtKey)
	}
	if err != nil {
		log.Printf("Problem creating cookie for %v", clientId)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name:     auth.ClientToken,
		Path:     "/",
		Value:    tokenString,
		Expires:  expirationTime,
		SameSite: http.SameSiteLaxMode,
	})
}

func (gm *Manager) DeleteRoom(w http.ResponseWriter, r *http.Request) {
	roomId := chi.URLParam(r, "roomId")
	userClaims, found, err := auth.ProcessUserToken(gm.JwtKey, r)
	if !found {
		err = http.ErrNoCookie
	}
	if err != nil {
		log.Printf("User claims error: %v\n", err)
		w.WriteHeader(http.StatusForbidden)
		return
	}
	if !userClaims.HasRole(HostRole) {
		log.Printf("User %v tried to delete a room with roles %v\n", userClaims.Username, userClaims.Roles)
		w.WriteHeader(http.StatusForbidden)
		return
	}
	room, ok := gm.Rooms[roomId]
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	room.Close()
	delete(gm.Rooms, roomId)
}

func (gm *Manager) CreateRoom(w http.ResponseWriter, r *http.Request) {
	userClaims, found, err := auth.ProcessUserToken(gm.JwtKey, r)
	if !found {
		err = http.ErrNoCookie
	}
	if err != nil {
		log.Printf("User claims error: %v\n", err)
		w.WriteHeader(http.StatusForbidden)
		return
	}
	if !userClaims.HasRole(HostRole) {
		log.Printf("User %v tried to create a room with roles %v\n", userClaims.Username, userClaims.Roles)
		w.WriteHeader(http.StatusForbidden)
		return
	}
	var info GameRoomInfo
	err = json.NewDecoder(r.Body).Decode(&info)
	if err != nil {
		log.Printf("Problem reading room info %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	gm.mu.Lock()
	defer gm.mu.Unlock()
	info.Id = gm.GenerateRoomId()
	info.Host = userClaims.Username
	room := CreateGameRoom(info)
	gm.Rooms[info.Id] = room
	if !room.AddClient(userClaims.Username) {
		log.Printf("Unable to add host %v to new room %v\n", userClaims.Username, info.Id)
	}
	tokenString, expirationTime, err :=
		auth.NewClientToken(info.Id, userClaims.Username, []string{HostRole, DisplayRole}, gm.JwtKey)
	if err != nil {
		log.Printf("Problem creating client token for %v: %v\n", userClaims.Username, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name:     auth.ClientToken,
		Path:     "/",
		Value:    tokenString,
		Expires:  expirationTime,
		SameSite: http.SameSiteLaxMode,
	})
	gm.renderer.JSON(w, 200, info)
}

func (gr *Manager) GenerateRoomId() string {
	var id string
	// do...while's in go are quirky
	for found := true; found; _, found = gr.Rooms[id] {
		id = strconv.Itoa(rand.Intn(10000))
	}
	return id
}

func (gm *Manager) ListRooms(w http.ResponseWriter, r *http.Request) {
	gm.mu.RLock()
	defer gm.mu.RUnlock()
	response := make([]GameRoomInfo, 0)
	for _, room := range gm.Rooms {
		response = append(response, room.Info)
	}
	gm.renderer.JSON(w, 200, response)
}

func (gm *Manager) GetInfo(roomId string) (info GameRoomInfo, ok bool) {
	gm.mu.RLock()
	defer gm.mu.RUnlock()
	room, ok := gm.Rooms[roomId]
	if ok {
		info = room.Info
	}
	return
}

func (gm *Manager) GetMemberships(w http.ResponseWriter, r *http.Request) {
	clientClaims, found, err := auth.ProcessClientToken(gm.JwtKey, r)
	if err != nil {
		log.Printf("Client token error: %v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	rval := make(map[string]string)
	if found {
		gm.mu.RLock()
		defer gm.mu.RUnlock()
		for roomId, membershipInfo := range clientClaims.RoomMap {
			room, ok := gm.Rooms[roomId]
			if ok {
				_, ok := room.Clients[membershipInfo.ClientId]
				if ok {
					rval[roomId] = membershipInfo.ClientId
				}
			}
		}
	}
	gm.renderer.JSON(w, 200, rval)
}
