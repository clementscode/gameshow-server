package auth

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/golang-jwt/jwt/v4"
)

const UserToken = "usertoken"
const ClientToken = "clienttoken"

type UserClaims struct {
	Username string   `json:"username"`
	Roles    []string `json:"roles"`
	jwt.RegisteredClaims
}

func (uc UserClaims) HasRole(role string) bool {
	return contains(role, uc.Roles)
}

func intersects(a []string, b []string) bool {
	for _, v := range a {
		if contains(v, b) {
			return true
		}
	}
	return false
}

func contains(value string, values []string) bool {
	for _, v := range values {
		if v == value {
			return true
		}
	}
	return false
}

type MemberInfo struct {
	RoomId   string   `json:"roomId"`
	ClientId string   `json:"clientId"`
	Roles    []string `json:"roles"`
}

func (mi MemberInfo) HasRole(role string) bool {
	return contains(role, mi.Roles)
}

type ClientClaims struct {
	RoomMap map[string]MemberInfo `json:"roomMap"`
	jwt.RegisteredClaims
}

func (c ClientClaims) AddMemberInfo(roomId, clientId string, roles []string) {
	c.RoomMap[roomId] = MemberInfo{roomId, clientId, roles}
}

func (c ClientClaims) IsMember(roomId, clientId string) bool {
	info, ok := c.RoomMap[roomId]
	return ok && info.ClientId == clientId
}

func (c ClientClaims) HasRole(roomId, clientId, role string) bool {
	info, ok := c.RoomMap[roomId]
	if !ok || info.ClientId != clientId {
		return false
	}
	return info.HasRole(role)
}

func NewUserToken(username string, roles []string, jwtKey []byte) (string, time.Time, error) {
	expirationTime := time.Now().Add(2 * time.Hour)
	claims := &UserClaims{
		Username: username,
		Roles:    roles,
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expirationTime),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	value, err := token.SignedString(jwtKey)
	return value, expirationTime, err
}

func NewClientToken(roomId, clientId string, roles []string, jwtKey []byte) (string, time.Time, error) {
	expirationTime := time.Now().Add(12 * time.Hour)
	claims := &ClientClaims{
		RoomMap: make(map[string]MemberInfo),
		RegisteredClaims: jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(expirationTime),
		},
	}
	claims.AddMemberInfo(roomId, clientId, roles)

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	value, err := token.SignedString(jwtKey)
	return value, expirationTime, err
}

func UpdateClientToken(roomId, clientId string, roles []string, claims ClientClaims, jwtKey []byte) (string, time.Time, error) {
	expirationTime := time.Now().Add(4 * time.Hour)
	claims.RegisteredClaims.ExpiresAt = jwt.NewNumericDate(expirationTime)
	claims.AddMemberInfo(roomId, clientId, roles)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	value, err := token.SignedString(jwtKey)
	return value, expirationTime, err
}

func ProcessUserToken(jwtKey []byte, r *http.Request) (claims UserClaims, found bool, err error) {
	found, err = ProcessToken(UserToken, &claims, jwtKey, r)
	return
}

func ProcessClientToken(jwtKey []byte, r *http.Request) (claims ClientClaims, found bool, err error) {
	found, err = ProcessToken(ClientToken, &claims, jwtKey, r)
	return
}

func ProcessToken(name string, claims jwt.Claims, jwtKey []byte, r *http.Request) (found bool, err error) {
	cookie, err := r.Cookie(name)
	if err != nil {
		if errors.Is(err, http.ErrNoCookie) {
			return false, nil
		}
		return true, err
	}
	token, err := jwt.ParseWithClaims(cookie.Value, claims, func(token *jwt.Token) (any, error) {
		return jwtKey, nil
	})
	if err != nil {
		return true, err
	}
	if !token.Valid {
		err = fmt.Errorf("Invalid token")
	}
	return true, err
}
