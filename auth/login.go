package auth

import (
	"encoding/json"
	"log"
	"net/http"
)

type WebAuth struct {
	UserStore
	JwtKey []byte
}

func NewWebAuth(store UserStore, jwtKey []byte) WebAuth {
	return WebAuth{store, jwtKey}
}

func (wa WebAuth) Login(w http.ResponseWriter, r *http.Request) {
	var loginReq User
	err := json.NewDecoder(r.Body).Decode(&loginReq)
	if err != nil {
		log.Printf("Problem reading login request %v\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	user, err := wa.GetUser(loginReq.Username)
	if err != nil {
		log.Printf("Can't get user %v\n", err)
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	if loginReq.Password != user.Password {
		log.Printf("Bad password\n")
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	tokenString, expirationTime, err := NewUserToken(user.Username, user.Roles, wa.JwtKey)
	http.SetCookie(w, &http.Cookie{
		Name:     UserToken,
		Path:     "/",
		Value:    tokenString,
		Expires:  expirationTime,
		SameSite: http.SameSiteLaxMode,
	})
}

func (wa WebAuth) WithRole(role string, inner http.HandlerFunc) http.HandlerFunc {
	return wa.WithRoles([]string{role}, inner)
}

func (wa WebAuth) WithRoles(roles []string, inner http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		claims, present, err := ProcessUserToken(wa.JwtKey, r)
		if !present || err != nil {
			log.Printf("Unable to get token: %v", err)
			w.WriteHeader(http.StatusForbidden)
			return
		}
		if intersects(roles, claims.Roles) {
			inner(w, r)
		} else {
			log.Printf("Unable to get token: %v", err)
			w.WriteHeader(http.StatusForbidden)
		}
	}
}
