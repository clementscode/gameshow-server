package auth

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type User struct {
	Username string   `json:"username"`
	Password string   `json:"password"`
	Roles    []string `json:"roles"`
}

type UserStore interface {
	GetUser(username string) (User, error)
}

type FileUserStore struct {
	Users map[string]User
}

func (f FileUserStore) GetUser(username string) (u User, e error) {
	u, ok := f.Users[username]
	if !ok {
		e = fmt.Errorf("Unknown user")
	}
	return
}

func CreateFileUserStore(path string) (store FileUserStore, err error) {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return
	}
	users := map[string]User{}
	err = json.Unmarshal(bytes, &users)
	if err == nil {
		for username, user := range users {
			user.Username = username
			users[username] = user
		}
		store.Users = users
	}
	return
}

func GetUserStore(name string, config map[string]string) (result UserStore, err error) {
	switch name {
	case "filesystem":
		usersFile, ok := config["usersFile"]
		if !ok {
			err = fmt.Errorf("Missing usersFile entry in config")
		} else {
			result, err = CreateFileUserStore(usersFile)
		}
	default:
		err = fmt.Errorf("Unsupported user storage: %s", name)
	}
	return
}
